from tern_rdf import TernRdf

from transforms import Regions, Subregions
from config import Config

import os

if __name__ == '__main__':

    g = TernRdf.Graph()

    regions = Regions(os.path.join(Config.dirpath, 'source_data', 'regions-list.xlsx'))
    regions.transform(g)
    subregions = Subregions(os.path.join(Config.dirpath, 'source_data', 'subregions-list.xlsx'))
    subregions.transform(g)

    g.serialize(os.path.join(Config.dirpath, 'ibra7.ttl'), format='turtle')

    # for index, row in df.iterrows():
    #     print(row['reg_code_7'])
    #     break