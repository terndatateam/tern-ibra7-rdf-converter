import pandas as pd
from rdflib import Literal, URIRef, BNode, Graph
from rdflib.namespace import RDF
from tern_rdf import TernRdf, SKOS, DWCT, BIOREGION, DCTERMS, SDO

from transforms.base import Base

from datetime import datetime

#
# Schema for "subregions-list.xlsx"
#
SUB_CODE_7 = 'SUB_CODE_7'  # Alphabetic identifier for the IBRA sub-region
SUB_NAME_7 = 'SUB_NAME_7'  # The full name of the IBRA sub-region
REG_CODE_7 = 'REG_CODE_7'  # Alphabetic identifier for the IBRA region
REG_NAME_7 = 'REG_NAME_7'  # The full name of the IBRA region
HECTARES = 'HECTARES'  # Area in Hectares
REC_ID = 'REC_ID'  # Numeric identifier for the IBRA region
SUB_CODE_6 = 'SUB_CODE_6'  # Alphabetic identifier of the previous IBRA6.1 sub-region
SUB_NAME_6 = 'SUB_NAME_6'  # The full name of the previous IBRA6.1 sub-region
SUB_NO_61_ = 'SUB_NO_61_'  # # Numeric identifier of the previous IBRA6.1 sub-region
REG_CODE_6 = 'REG_CODE_6'  # Alphabetic identifier of the previous IBRA6.1 region
REG_NAME_6 = 'REG_NAME_6'  # The full name of the previous IBRA6.1 region
REG_NO_61 = 'REG_NO_61'  # Numeric identifier of the previous IBRA6.1 region


class Subregions(Base):
    def __init__(self, filepath):
        self.filepath = filepath

    def transform(self, g: Graph):
        df = pd.read_excel(self.filepath)
        NS = BIOREGION

        # Create Concept Scheme
        region_id = 'ibra7'
#         g.add((NS[region_id], RDF.type, SKOS.ConceptScheme))
#         g.add((NS[region_id], DCTERMS.title, Literal('Interim Biogeographic Regionalisation for Australia (Subregions) v. 7 (IBRA)', lang='en')))
#         g.add((NS[region_id], DCTERMS.subject, Literal('IBRA7 Subregion Codes', lang='en')))
#         g.add((NS[region_id], DCTERMS.source, Literal("""Department of the Environment (2012), Interim Biogeographic Regionalisation for Australia (Subregions) v. 7 (IBRA) [ESRI shapefile]
# Available from http://intspat01.ris.environment.gov.au/fed/catalog/search/resource/details.page?uuid=%7BDFB470D2-FA7E-4792-B524-0250EC3AF5B3%7D""", lang='en')))
#
#         # g.add((NS['ibra7'], DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
#         g.add((NS[region_id], DCTERMS.creator, BNode('creator')))
#         g.add((BNode('creator'), RDF.type, SDO.Person))
#         g.add((BNode('creator'), SDO.identifier, URIRef('https://orcid.org/0000-0002-6047-9864')))
#         g.add((BNode('creator'), SDO.email, Literal('e.chuc@uq.edu.au', lang='en')))
#         g.add((BNode('creator'), SDO.name, Literal('Edmond Chuc', lang='en')))
#         g.add((BNode('creator'), SDO.worksFor,
#                URIRef('http://linkeddata.tern.org.au/def/agent/a083902d-d821-41be-b663-1d7cb33eea66')))

        g.add((NS[region_id], DCTERMS.publisher,
               URIRef('http://linkeddata.tern.org.au/def/agent/a083902d-d821-41be-b663-1d7cb33eea66')))
        g.add((NS[region_id], DCTERMS.created, Literal(datetime.now())))

        for index, row in df.iterrows():
            # Add properties to Concept Scheme
            # g.add((NS[region_id], SKOS.hasTopConcept, NS[row[SUB_CODE_7]]))
            # g.add((NS[row[SUB_CODE_7]], SKOS.topConceptOf, NS[region_id]))
            # These are subregions, they should not be top concepts. They should have broaders to a region.

            # Add metadata properties to Concept
            g.add((NS[row[SUB_CODE_7]], RDF.type, SKOS.Concept))
            g.add((NS[row[SUB_CODE_7]], RDF.type, DWCT.MeasurementOrFact))
            g.add((NS[row[SUB_CODE_7]], SKOS.inScheme, NS[region_id]))
            g.add((NS[row[SUB_CODE_7]], SKOS.notation, Literal(row[SUB_CODE_7])))
            g.add((NS[row[SUB_CODE_7]], SKOS.prefLabel, Literal(row[SUB_NAME_7], lang='en')))

            g.add((NS[row[SUB_CODE_7]], SKOS.broader, NS[row[REG_CODE_7]]))
            g.add((NS[row[REG_CODE_7]], SKOS.narrower, NS[row[SUB_CODE_7]]))

            g.add((NS[row[SUB_CODE_7]], DWCT.measurementType, Literal('Subregion area.', lang='en')))
            g.add((NS[row[SUB_CODE_7]], DWCT.measurementUnit, URIRef('http://qudt.org/vocab/unit/HA')))
            g.add((NS[row[SUB_CODE_7]], DWCT.measurementAccuracy, Literal(
                'Less than two kilometres in most areas of Australia. Generally, the data is accurate to at least 1:250,000', lang='en')))
            g.add((NS[row[SUB_CODE_7]], DWCT.measurementValue, Literal(row[HECTARES])))
            g.add((NS[row[SUB_CODE_7]], SKOS.hiddenLabel, Literal('REC_ID {}'.format(row[REC_ID]), lang='en')))
            g.add((NS[row[SUB_CODE_7]], SKOS.hiddenLabel, Literal('REG_NO_61 {}'.format(row[REG_NO_61]), lang='en')))

        # g.serialize(os.path.join(Config.dirpath, 'ibra7-subregions.ttl'), format='turtle')