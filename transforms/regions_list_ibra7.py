import pandas as pd
from rdflib import Literal, URIRef, BNode, Graph
from rdflib.namespace import RDF
from tern_rdf import TernRdf, SKOS, DWCT, BIOREGION, DCTERMS, SDO

from transforms.base import Base
from config import Config

from datetime import datetime
import os

#
# Schema for "regions-list.xlsx"
#
REG_CODE_7 = 'REG_CODE_7'  # Alphabetic identifier for the IBRA region
REG_NAME_7 = 'REG_NAME_7'
HECTARES = 'HECTARES'  # Area in Hectares
REG_CODE_6 = 'REG_CODE_6'  # Alphabetic identifier of the previous IBRA6.1 region
REG_NO_61 = 'REG_NO_61'  # Numeric identifier of the previous IBRA6.1 region


class Regions(Base):
    def __init__(self, filepath):
        self.filepath = filepath

    def transform(self, g: Graph):
        df = pd.read_excel(self.filepath)
        NS = BIOREGION

        # Create Concept Scheme
        region_id = 'ibra7'
        g.add((NS[region_id], RDF.type, SKOS.ConceptScheme))
        g.add((NS[region_id], DCTERMS.title, Literal('Interim Biogeographic Regionalisation for Australia v. 7 (IBRA)', lang='en')))
        g.add((NS[region_id], DCTERMS.description, Literal('This vocabulary contains the region and subregion codes for the IBRA.')))
        g.add((NS[region_id], DCTERMS.subject, Literal('IBRA7 Region and Subregion Codes', lang='en')))
        g.add((NS[region_id], DCTERMS.source, Literal("""Department of the Environment (2012), Interim Biogeographic Regionalisation for Australia v. 7 (IBRA) [ESRI shapefile] 
Available from http://intspat01.ris.environment.gov.au/fed/catalog/search/resource/details.page?uuid=%7B3C182B5A-C081-4B56-82CA-DF5AF82F86DD%7D""", lang='en')))

        # g.add((NS['ibra7'], DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
        g.add((NS[region_id], DCTERMS.creator, BNode('creator')))
        g.add((BNode('creator'), RDF.type, SDO.Person))
        g.add((BNode('creator'), SDO.identifier, URIRef('https://orcid.org/0000-0002-6047-9864')))
        g.add((BNode('creator'), SDO.email, Literal('e.chuc@uq.edu.au', lang='en')))
        g.add((BNode('creator'), SDO.name, Literal('Edmond Chuc', lang='en')))
        g.add((BNode('creator'), SDO.worksFor,
               URIRef('http://linkeddata.tern.org.au/def/agent/a083902d-d821-41be-b663-1d7cb33eea66')))

        g.add((NS[region_id], DCTERMS.publisher,
               URIRef('http://linkeddata.tern.org.au/def/agent/a083902d-d821-41be-b663-1d7cb33eea66')))
        g.add((NS[region_id], DCTERMS.created, Literal(datetime.now())))

        for index, row in df.iterrows():
            # Add properties to Concept Scheme
            g.add((NS[region_id], SKOS.hasTopConcept, NS[row[REG_CODE_7]]))
            g.add((NS[row[REG_CODE_7]], SKOS.topConceptOf, NS[region_id]))

            # Add metadata properties to Concept
            g.add((NS[row[REG_CODE_7]], RDF.type, SKOS.Concept))
            g.add((NS[row[REG_CODE_7]], RDF.type, DWCT.MeasurementOrFact))
            g.add((NS[row[REG_CODE_7]], SKOS.inScheme, NS[region_id]))
            g.add((NS[row[REG_CODE_7]], SKOS.notation, Literal(row[REG_CODE_7])))
            g.add((NS[row[REG_CODE_7]], SKOS.prefLabel, Literal(row[REG_NAME_7], lang='en')))
            g.add((NS[row[REG_CODE_7]], SKOS.hiddenLabel, Literal('REG_NO_61 {}'.format(row[REG_NO_61]), lang='en')))

            # Add measurement data to Concept
            g.add((NS[row[REG_CODE_7]], DWCT.measurementType, Literal('Region area.', lang='en')))
            g.add((NS[row[REG_CODE_7]], DWCT.measurementUnit, URIRef('http://qudt.org/vocab/unit/HA')))
            g.add((NS[row[REG_CODE_7]], DWCT.measurementAccuracy, Literal(
                'Less than two kilometres in most areas of Australia. Generally, the data is accurate to at least 1:250,000', lang='en')))
            g.add((NS[row[REG_CODE_7]], DWCT.measurementValue, Literal(row[HECTARES])))

            g.add((NS[row[REG_CODE_7]], DCTERMS.created, Literal(datetime.now())))

        # g.serialize(os.path.join(Config.dirpath, 'ibra7-regions.ttl'), format='turtle')
