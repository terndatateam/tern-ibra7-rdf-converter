# Modelling the Interim Biogegraphic Regions of Australia (IBRA7) 
The purpose of this repository is to take the *interim biogeographic regionalisation for Australia (IBRA7) codes*, modelling it using SKOS, Dublin Core Terms, and Darwin Core Terms, transforming it into a controlled vocabulary, and serving it online as Linked Data. 

The source data for both regions and sub-regions were retrieved from:
- https://www.environment.gov.au/land/nrs/science/ibra/ibra7-codes

The metadata for IBRA7 regions was retrieved from:
- http://www.environment.gov.au/fed/catalog/search/resource/details.page?uuid=%7B4A2321F0-DD57-454E-BE34-6FD4BDE64703%7D

The metadata for IBRA7 sub-regions was retrieved from:
- http://www.environment.gov.au/fed/catalog/search/resource/details.page?uuid=%7B8B9E3F42-9856-4487-AE9E-C76A322809A1%7D


## Mapping Table for IBRA7 Regions List

| Source Column | OWL Class              | OWL Property                                                                                   |
|---------------|------------------------|------------------------------------------------------------------------------------------------|
| REG_CODE_7    | skos:Concept           | skos:notation                                                                                  |
| REG_NAME_7    | skos:Concept           | skos:prefLabel                                                                                 |
| HECTARES      | dwct:MeasurementOrFact | dwct:measurementType,  dwct:measurementUnit,  dwct:measurementAccuracy,  dwct:measurementValue |
| REG_NO_61     | skos:Concept           | skos:hiddenLabel - the value will be "REG_NO_61 {}"                                            |


## Mapping Table for IBRA7 Sub-Regions List

| Source Column | OWL Class              | OWL Property                                                                                  |
|---------------|------------------------|-----------------------------------------------------------------------------------------------|
| SUB_CODE_7    | skos:Concept           | skos:notation                                                                                 |
| SUB_NAME_7    | skos:Concept           | skos:prefLabel                                                                                |
| REG_CODE_7    | skos:Concept           | skos:broadMatch to a regions list concept                                                        |
| HECTARES      | dwct:MeasurementOrFact | dwct:measurementType,  dwct:measurementUnit,  dwct:measurementAccuracy, dwct:measurementValue |
| REC_ID        | skos:Concept           | skos:hiddenLabel - the value will be "REC_ID {}"                                              |
| REG_NO_61     | skos:Concept           | skos:hiddenLabel - the value will be "REG_NO_61 {}"                                           |


## Author
Edmond Chuc  
Software Engineer  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
